from PyQt5.QtWidgets import QMainWindow, QApplication, \
    QFileDialog, QMessageBox, QInputDialog

from PyQt5.QtGui import QPixmap, QImage
from PyQt5.QtCore import QObject, QEvent
from PyQt5.Qt import Qt

from MainWindowUi import Ui_MainWindow

import cv2.cv2 as cv
import numpy as np

import math
import os

import image_functions as images


from typing import Generator, Optional


class MainWindow(QMainWindow, Ui_MainWindow):
    """
    Оконное приложение для разметки датасета
    и его сохранения в правильном формате
    """

    images: Generator   # Генератор изображений

    current_image: np.ndarray   # Текущее изображение
    current_mask: np.ndarray    # Текущая маска

    actions: list           # Список построенных полигонов
    undone_actions: list    # Список отмененных действий

    index: int      # Индекс текущего изображения
    dir_out: str    # Директория сохранения

    current_poly = Optional[list]   # Текущий полигон

    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)
        self.setupUi(self)

        # Соединение сигналов и слотов
        self.action_select_folders.triggered.connect(self.open_folders)
        self.button_skip.clicked.connect(self.next_image)
        self.button_next.clicked.connect(self.save_next)

        self.action_undo.triggered.connect(self.undo)
        self.action_redo.triggered.connect(self.redo)

        # Подключение фильтра событий для обнаружения нажатий на изображение
        self.image_label.installEventFilter(self)

        # Начало работы
        self.open_folders()

    def open_folders(self):
        """Выбор папок и инициализация генератора изображений"""

        # Выбор папки входных изображений
        dir_in = QFileDialog.getExistingDirectory(
            self, 'Where to load images from?', './input'
        )

        if dir_in:
            # Инициализация генератора
            self.images = images.load_bunch_named(os.path.join(dir_in, '*.*'))
        else:
            return

        # Выбор папки для вывода
        dir_out = QFileDialog.getExistingDirectory(
            self, 'Where to save masks to?', './input'
        )

        if dir_out:
            self.dir_out = dir_out
        else:
            return

        # Активация кнопок
        self.button_next.setEnabled(True)
        self.button_skip.setEnabled(True)

        # Получение индекса для нумерации
        ind, _ = QInputDialog.getInt(self, 'Start index',
                                     'What index to start from?', 0, 0)

        self.index = ind

        # Отображение первого изображения
        self.next_image()

    def save_next(self):
        """Сохранить текущую разметку и перейти к следующему изображению"""
        if self.current_image is not None:

            # Компиляция маски
            self.compile_mask(final=True)

            # Сохранение изображения
            cv.imwrite(
                os.path.join(self.dir_out, f'img_{self.index}.png'),
                cv.hconcat([self.current_image, self.current_mask])
            )

            self.index += 1

        # Переход к следующему изображению
        self.next_image()

    def next_image(self):
        """Загрузка очередного изображения или предупреждение о завершении"""
        try:
            # Получаем имя изображения и изображение
            filename, self.current_image = next(self.images)

            # Задаём начальные значения маске и спискам действий
            self.prepare_mask()

            # Отображаем изображение на экране
            self.display_image()

            # Отображение имени файла в строке состояния
            self.statusbar.showMessage(filename)

        except StopIteration:   # При истощении генератора

            # Блокировка кнопок
            self.button_next.setEnabled(False)
            self.button_skip.setEnabled(False)

            # Отображение сообщения о завершении
            QMessageBox.information(self, 'Finished!',
                                    'All images have been processed!')

            # Очистка строки состояния
            self.statusbar.showMessage('')

    def display_image(self):
        """Накладывает маску на изображение и отображает его на экране"""

        # Наложение маски
        image = cv.add(self.current_image, self.current_mask)

        # Получение размеров изображения
        H, W, _ = image.shape

        # Преобразование изображения в QPixmap
        q_image = QImage(image.data, W, H, 3 * W, QImage.Format_BGR888)
        pixmap = QPixmap.fromImage(q_image)

        # Отображение
        self.image_label.setPixmap(pixmap)

    def redisplay_image(self):
        """Заново собирает маску и отображает текущее изображение"""
        self.compile_mask()
        self.display_image()

    def prepare_mask(self):
        """Задаёт начальные значения для новой маски"""

        # Маска - массив нулей по форме изображения
        self.current_mask = np.zeros_like(self.current_image)

        # Списки действий
        self.actions = []
        self.undone_actions = []
        self.current_poly = []

        # Отключение "отменить" и "вернуть изменения"
        self.action_undo.setEnabled(False)
        self.action_redo.setEnabled(False)

    def compile_mask(self, *, final=False):
        """Сборка маски из нарисованных полигонов"""

        # Создание пустой маски по форме изображения
        self.current_mask = np.zeros_like(self.current_image)

        for poly in self.actions:   # Для каждого полигона
            # Отобразить полигон белым цветом
            cv.fillPoly(self.current_mask, [poly], (255, 255, 255))

        if not final:   # Если не финальная сборка
            if self.current_poly:   # Отображение недостроенного полигона

                # Отображение уже построенных граней
                poly = np.array(self.current_poly, np.int32)
                cv.polylines(self.current_mask, [poly], False, (255, 255, 255))

                # Отображение круга в последней установленной точке
                last = self.current_poly[-1]
                cv.circle(self.current_mask, last, 3, (255, 255, 255), 1)

    def undo(self):
        """Отменяет последний полигон"""

        if self.current_poly:   # Если есть незавершенный полигон
            self.current_poly.clear()   # Удалить его

            # Запретить отмену, если нечего отменять
            if not self.actions:
                self.action_undo.setEnabled(False)

        elif self.actions:  # Если есть что отменять

            # Переместить последнее действие в отмененные
            self.undone_actions.append(self.actions.pop(-1))

            # Разрешить возврат отмененных действий
            self.action_redo.setEnabled(True)

            # Запретить отмену, если нечего отменять
            if not self.actions:
                self.action_undo.setEnabled(False)

        # Перерисовать изображение с маской
        self.redisplay_image()

    def redo(self):
        """Восстанавливает отмененный полигон"""

        if self.undone_actions:  # Если есть отмененные действия

            # Перенести последнее отмененное действие в совершенные действия
            self.actions.append(self.undone_actions.pop(-1))

            # Разрешить отмену
            self.action_undo.setEnabled(True)

            # Запретить восстановление, если больше нет отмененных действий
            if not self.undone_actions:
                self.action_redo.setEnabled(False)

        # Перерисовать изображение с маской
        self.redisplay_image()

    def eventFilter(self, obj: QObject, event: QEvent) -> bool:
        """Фильтр событий для обнаружения нажатий на изображение"""

        # Выбор события нажатия кнопки мыши для изображения
        if obj is self.image_label:
            if event.type() == QEvent.MouseButtonPress:

                # Получение координат нажатия
                pos = event.localPos()
                point = int(pos.x()), int(pos.y())

                # Обработка события
                self.image_clicked(point, event.button() == Qt.RightButton)

                return True

        return super(MainWindow, self).eventFilter(obj, event)

    def image_clicked(self, point, right=False):
        """Обработка нажатия на изображение (рисование)"""
        close = False   # Нужно ли закрывать полигон

        # Если в полигоне достаточно точек и новая точка находится близко к
        # открывающей, полигон нужно закрыть
        if len(self.current_poly) > 2 \
                and math.dist(point, self.current_poly[0]) <= 10:

            close = True

        else:
            # Иначе, добавляем новую точку
            self.current_poly.append(point)

        # Если нажата правая кнопка, то полигон тоже нужно закрыть
        close = close or right

        # Если в полигоне достаточно точек и его нужно закрыть
        if len(self.current_poly) > 2 and close:
            # Полигон добавляется в совершенные действия
            self.actions.append(np.array(self.current_poly, np.int32))

            # Текущий полигон удаляется
            self.current_poly.clear()

            # Очищается список отмененных действий
            self.action_redo.setEnabled(False)
            self.undone_actions.clear()

        # Разрешается отмена
        self.action_undo.setEnabled(True)

        # Перерисовка изображения
        self.redisplay_image()


if __name__ == '__main__':
    a = QApplication([])
    w = MainWindow()

    w.show()
    a.exec()
