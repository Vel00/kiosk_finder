"""
Функции для работы с изображениями

Некоторые функции были использованы прежде,
но были заменены в связи с различными обстоятельствами
"""

import cv2.cv2 as cv
import numpy as np

import os
import glob

from tqdm import trange


from typing import Iterable


FRAGMENT_SIZE = 128


def load_bunch(pathname: str):
    """
    Загрузка файлов изображений, которые соответствуют шаблону pathname

    :param pathname: шаблон имени файла (например 'input/img_*.png')
    """
    files = glob.glob(pathname)

    if not files:
        raise FileNotFoundError('Нет подходящих изображений')

    for filename in files:
        image = cv.imread(filename)  # Чтение изображения

        if image is not None:
            yield image  # Возврат изображения


def estimate_bunch(pathname: str):
    files = glob.glob(pathname)

    return len(files)


def load_bunch_named(pathname: str):
    """
    Загрузка файлов изображений, которые соответствуют шаблону pathname.
    Возвращает имя файла вместе с изображением

    :param pathname: шаблон имени файла (например 'input/img_*.png')
    """
    files = glob.glob(pathname)

    if not files:
        raise FileNotFoundError('Нет подходящих изображений')

    for filename in files:
        image = cv.imread(filename)

        if image is not None:
            yield filename, image


def save_bunch(bunch: Iterable, directory: str = 'output',
               prefix: str = '', suffix: str = '', ext: str = 'png'):
    """
    Сохраняет изображения, задавая имена в соответствии с параметрами

    :param bunch: итератор изображений
    :param directory: директория сохранения
    :param prefix: префикс имени файла
    :param suffix: суффикс имени файла
    :param ext: расширение файла
    """

    # Создание директории сохранения, если она не существует
    if not os.path.exists(directory):
        os.mkdir(directory)

    # Сохранение изображений
    for i, img in enumerate(bunch):
        filename = os.path.join(directory, f'{prefix}{i}{suffix}.{ext}')
        cv.imwrite(filename, img)


def variate_image(img: np.ndarray):
    """
    Создаёт вариации изображения при помощи вращения и отражения

    :param img: изображение для вариации
    """
    yield img   # Исходное изображение
    yield cv.flip(img, 1)   # Отразить по горизонтали

    tmp = cv.rotate(img, cv.ROTATE_90_CLOCKWISE)
    yield tmp   # 90 градусов по часовой
    yield cv.flip(tmp, 1)   # Отразить повернутое изображение

    tmp = cv.rotate(img, cv.ROTATE_180)
    yield tmp   # 180 градусов
    yield cv.flip(tmp, 1)   # Отразить повернутое изображение

    tmp = cv.rotate(img, cv.ROTATE_90_COUNTERCLOCKWISE)
    yield tmp   # 270 градусов (90 против часовой)
    yield cv.flip(tmp, 1)   # Отразить повернутое изображение


def jigsaw(img: np.ndarray, size: int = FRAGMENT_SIZE):
    """
    Разделяет изображение на фрагменты размера size x size

    :param img: исходное изображение
    :param size: сторона фрагмента
    """
    H, W = img.shape[:2]    # Размеры исходного изображения

    # Итерация вертикально, по строкам высотой size
    for h in range(0, H - size + 1, size):
        # Итерация горизонтально, по столбцам шириной size
        for w in range(0, W - size + 1, size):

            # Возвращение фрагмента в виде последовательного массива
            yield np.ascontiguousarray(img[h:h+size, w:w+size])


def split_two(img: np.ndarray):
    """
    Разделяет изображение на две части

    :param img: исходное изображение
    """

    w = img.shape[1] // 2  # Половина ширины изображения

    return img[:, :w, ...], img[:, w:, ...]


def prepare_image(image: np.ndarray, fragment_size: int = FRAGMENT_SIZE, *,
                  grayscale: bool = False, variate: bool = True):
    """
    Подготавливает изображение к использованию в нейросети:
     - Преобразует в оттенки серого, если grayscale - true
     - Разделяет изображение на фрагменты со стороной fragment_size
     - Создаёт вариации (вращение и отражение) если variate - true

    :param image: исходное изображение
    :param fragment_size: сторона фрагмента
    :param grayscale: преобразовывать в оттенки серого
    :param variate: варьировать
    """

    # Преобразовать в оттенки серого, если необходимо
    if grayscale:
        image = cv.cvtColor(image, cv.COLOR_BGR2GRAY)

    # Разделение на фрагменты
    for fragment in jigsaw(image, fragment_size):
        if variate:

            # Вариации
            for variant in variate_image(fragment):
                yield variant
        else:
            yield fragment


def reassemble(array: np.ndarray, original_shape: tuple,
               fragment_size: int = FRAGMENT_SIZE):
    """
    Восстанавливает изображение из фрагментов

    :param array: массив фрагментов изображения
    :param original_shape: исходный размер изображения
    :param fragment_size: размер фрагмента
    """

    H, W = original_shape[:2]

    # Получаем размер изображения в фрагментах
    H //= fragment_size
    W //= fragment_size

    # Выстраиваем фрагменты в матрицу
    new_array = array.reshape((H, W) + array.shape[1:])

    # Соединение строк и столбцов матрицы фрагментов
    step_1 = np.concatenate(new_array, axis=1)
    step_2 = np.concatenate(step_1, axis=1)

    return step_2


def gen_data(directory: str, fragment_size: int = FRAGMENT_SIZE):
    """
    Генератор данных для нейросети

    :param directory: директория, содержащая датасет
    :param fragment_size: размер фрагмента, принимаемый нейросетью
    """

    # Загрузка изображений
    try:
        for item in load_bunch(os.path.join(directory, 'img_*.png')):
            img, msk = split_two(item)  # Разделение изображения и маски

            # Подготовка изображений
            for x, y in zip(prepare_image(img, fragment_size),
                            prepare_image(msk, fragment_size)):

                # Нормализация значений входного массива
                x = x.astype(np.float32) / 255

                # Приведение значений выходного массива к формату one-hot
                y = bgr_to_onehot(y)

                yield x, y

    except FileNotFoundError as e:
        raise FileNotFoundError('Указанная директория не '
                                'содержит подходящих файлов') from e


def gen_data_batched(directory: str, fragment_size: int = FRAGMENT_SIZE):
    """
    Генератор данных для нейросети (Но в отличие от предыдущего, возвращает
    фрагменты массивами - один массив на изображение)

    :param directory: директория, содержащая датасет
    :param fragment_size: размер фрагмента, принимаемый нейросетью
    """

    # Загрузка изображений
    try:
        for item in load_bunch(os.path.join(directory, 'img_*.png')):
            img, msk = split_two(item)  # Разделение изображения и маски

            xs, ys = [], []

            # Подготовка изображений
            for x, y in zip(prepare_image(img, fragment_size),
                            prepare_image(msk, fragment_size)):

                # Нормализация значений входного массива
                x = x.astype(np.float32) / 255

                # Приведение значений выходного массива к формату one-hot
                y = bgr_to_onehot(y)

                xs.append(x)
                ys.append(y)

            yield xs, ys

    except FileNotFoundError as e:
        raise FileNotFoundError('Указанная директория не '
                                'содержит подходящих файлов') from e


def get_data(directory: str, fragment_size: int = FRAGMENT_SIZE):
    """
    Загружает данные для обучения нейросети в виде массивов

    :param directory: директория, содержащая датасет
    :param fragment_size: размер фрагмента, принимаемый нейросетью
    """

    # Используем генератор, разделяя x и y по разным спискам
    x, y = [], []
    gen = gen_data_batched(directory, fragment_size)

    ds_size = estimate_bunch(os.path.join(directory, 'img_*.png'))

    for _ in trange(ds_size, desc='Loading images'):
        try:
            xi, yi = next(gen)

            x.extend(xi)
            y.extend(yi)

        except StopIteration:
            print('Did you just deleted file from dataset???')
            pass

    # Преобразуем списки в массивы соответствующих типов
    return np.array(x, dtype=np.float32), np.array(y, dtype=np.int8)


colors = [(0, 0, 0),  # Ничего нет
          (255, 255, 255)  # Киоск/павильон
          ]


def bgr_to_onehot(bgr: np.ndarray) -> np.ndarray:
    """
    Преобразует изображение к формату one-hot

    :param bgr: исходное изображение
    """

    num_classes = len(colors)  # Количество цветовых кодов
    shape = bgr.shape[:2] + (num_classes,)  # Форма выходного массива

    arr = np.zeros(shape, dtype=np.int8)  # Выходной массив, заполненный нулями

    # Заполнение слоёв соответствующих цветовым кодам
    for i, color in enumerate(colors):
        # Заполнение единицами ячеек i-го слоя,
        arr[:, :, i] = np.all(bgr.reshape((-1, 3)) == color, axis=1)\
            .reshape(shape[:2])

    return arr


def onehot_to_bgr(onehot: np.ndarray) -> np.ndarray:
    """
    Преобразует изображение из формата one-hot

    :param onehot: изображение в формате one-hot
    """
    # Индексация массива в виде одного слоя
    single_layer = np.argmax(onehot, axis=-1)

    # Выходной массив, заполненный нулями
    output = np.zeros(onehot.shape[:2]+(3,))

    # Замена цветовых кодов цветами
    for i, color in enumerate(colors):
        output[single_layer == i] = color

    return np.uint8(output)


def crop(image: np.ndarray, target_shape):
    w, h = target_shape[:2]

    return image[:w, :h, ...].copy()


if __name__ == '__main__':
    get_data('input/eval_labeled')
