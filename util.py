"""Разные полезные функции"""

from datetime import datetime
from time import time

import os

_print = print


class Logger:
    """Дублирует вывод на консоль записью в файл"""

    def __init__(self, filename):
        self.file = open(filename, 'w', encoding='utf-8', buffering=1)
        self.log('Logger started')

    def log(self, *args, **kwargs):
        t = f'[{datetime.fromtimestamp(time()):%D %H:%M:%S}]'

        _print(t, *args, **kwargs, file=self.file)
        _print(t, *args, **kwargs)

    def close(self):
        self.log('Logger finished')
        self.file.close()


def ensure_folders(*folders):
    """
    Убеждается, что все папки существуют

    :param folders: необходимые папки
    """

    for path in folders:
        if not os.path.exists(path) or not os.path.isdir(path):
            os.mkdir(path)


def ensure_new(path):
    """
    Убеждается, что будет создан новый файл (добавляет (1), (2) и т.д.)

    :param path: создаваемый файл
    """

    name, ext = os.path.splitext(path)
    i = 0

    while os.path.exists(path):
        i += 1
        path = f'{name}({i}){ext}'

    return path
