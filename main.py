import tensorflow as tf
from tensorflow.keras.models import Model
from tensorflow.keras.callbacks import Callback

import cv2.cv2 as cv
import numpy as np
import seaborn as sns
from matplotlib import pyplot as plt

import image_functions as images
from unet import UNet
from util import Logger, ensure_folders, ensure_new

import traceback
from datetime import datetime
from time import time
import json
import os


# Настройка стилей графиков
sns.set_style('darkgrid')
plt.rc('axes', titlesize=18)
plt.rc('axes', labelsize=14)
plt.rc('xtick', labelsize=13)
plt.rc('ytick', labelsize=13)
plt.rc('legend', fontsize=13)
plt.rc('font', size=13)

# --------------------------------------------------


# Разрешение увеличивать количество выделенной видеопамяти
physical_devices = tf.config.list_physical_devices('GPU')
try:
    tf.config.experimental.set_memory_growth(physical_devices[0], True)

except Exception:

    traceback.print_exc()
    pass

# --------------------------------------------------


# Задаём форму фрагмента на основе размера
fs = images.FRAGMENT_SIZE
image_shape = (fs, fs, 3)

BATCH_SIZE = 32

# Пути по умолчанию,
#  откуда программа загружает картинки и куда сохраняет результаты

PATH_INPUT = 'input'    # Входные изображения

# Подпапка для тренировочного датасета (размеченного)
PATH_TRAIN = os.path.join(PATH_INPUT, 'train_labeled')

# Подпапка для оценочного датасета (размеченного)
PATH_EVAL = os.path.join(PATH_INPUT, 'eval_labeled')

# Подпапка для тестового датасета (неразмеченного)
PATH_TEST = os.path.join(PATH_INPUT, 'test_images')

PATH_OUTPUT = 'output'  # Выходные изображения

PATH_MODEL = 'model'    # Чекпоинты модели и логи тренировки

# Подпапка для финального чекпоинта и статистики
PATH_MODEL_OVERALL = os.path.join(PATH_MODEL, 'overall')

# Создать папки, если они не существуют
ensure_folders(PATH_OUTPUT, PATH_MODEL, PATH_MODEL_OVERALL)

# Заменяем print на функцию логгера
log_file = ensure_new(os.path.join(PATH_MODEL, 'log.txt'))
logger = Logger(log_file)
print = logger.log

# --------------------------------------------------


def fit(model: Model = None, data_path=None,
        history: bool = False, verbose=False, **kwargs):
    """
    Тренировка модели на заданном датасете с заданными параметрами

    :param model: Модель для тренировки или None
    :param data_path: Директория, содержащая датасет
    :param history: Возвращать ли историю тренировки
    :param verbose: Выводить ли сообщения на консоль
    :param kwargs: Параметры тренировки
    """

    data_path = data_path or PATH_TRAIN  # Задать путь по умолчанию

    # Задание параметров, не заданных при вызове
    kwargs.setdefault('epochs', 10)
    kwargs.setdefault('batch_size', BATCH_SIZE)
    kwargs.setdefault('validation_split', 0.2)

    # Загрузка датасета
    verbose and print(f'Fit: Loading dataset from {data_path}')

    x_train, y_train = images.get_data(data_path)

    verbose and print('Fit: Dataset loaded')

    # Создание модели, если не была предоставлена
    if not model:
        verbose and print('Fit: No model provided. Creating...')

        model = UNet(image_shape)

    # Тренировка модели
    verbose and print('Fit: Start training')

    hst = model.fit(x_train, y_train, **kwargs)

    verbose and print('Fit: Finished training')

    if history:
        return hst, model

    return model


def evaluate(model: Model, data_path=None) -> dict:
    """
    Оценка модели

    :param model: Модель для оценки
    :param data_path: Директория, содержащая датасет
    """
    data_path = data_path or PATH_EVAL  # Задать путь по умолчанию

    # Загрузка датасета
    x_test, y_test = images.get_data(data_path)

    # Оценка модели
    return model.evaluate(x_test, y_test,
                          batch_size=BATCH_SIZE,
                          return_dict=True)


def predict(model: Model, image: np.ndarray, **kwargs) -> np.ndarray:
    """
    Генерация маски для заданного изображения с использованием модели

    :param model: Модель для генерации предсказания
    :param image: Изображение для предсказания
    """

    # Преобразование изображения к формату, принимаемому нейросетью
    parts = np.array([*images.prepare_image(image, variate=False)],
                     dtype=np.float32) / 255

    # Получение предсказания
    returns = model.predict(parts, batch_size=BATCH_SIZE, **kwargs)

    r = returns.shape[0]

    # Создание массива результатов
    results = np.zeros(returns.shape[:3] + (3,), dtype=np.uint8)

    # Преобразование предсказания из one-hot формата
    for i in range(r):
        results[i] = images.onehot_to_bgr(returns[i])

    # Сборка фрагментов в изображение
    return images.reassemble(results, image.shape)


class SavingCallback(Callback):
    """Обратный вызов для сохранения весов модели каждые n эпох"""

    epoch: int      # Шаг
    save_location: str      # Расположение для сохранения
    test_image: np.ndarray  # Изображение для тестирования
    do_evaluate: bool      # Проводить ли оценку
    continue_from: int      # С какой эпохи продолжаем

    def __init__(self, epoch=10, save_location=None, *, continue_from=0,
                 test_image=None, do_evaluate=True):
        """
        Обратный вызов для сохранения весов модели каждые n эпох

        :param epoch: Шаг сохранения
        :param save_location: Расположение для сохранения (def. PATH_MODEL)
        :param test_image: Изображение для проверки
        :param do_evaluate: Проводить ли оценку
        """

        super(SavingCallback, self).__init__()

        self.epoch = epoch
        self.save_location = save_location or PATH_MODEL

        self.continue_from = continue_from
        self.test_image = test_image
        self.do_evaluate = do_evaluate

    def on_epoch_end(self, epoch, logs=None):
        """
        Выполняется по завершении каждой эпохи.
        Сохраняет веса для каждой n-ой эпохи.

        :param epoch: Текущая эпоха
        :param logs: Объект логов (детали реализации keras)
        """
        epoch += 1  # Превращаем индекс в порядковый номер

        if not epoch % self.epoch:  # Проверяем, является эпоха каждой n-ой

            # Определяем папку эпохи
            output = os.path.join(self.save_location,
                                  f'e{self.continue_from + epoch}')

            if not os.path.exists(output):
                os.mkdir(output)

            print(f'Callback: Epoch {self.continue_from + epoch}')
            print(f'Callback: Saving weights')

            # Сохранение весов модели
            self.model.save_weights(os.path.join(output, 'weights'))

            print('Callback: Weights saved')

            try:
                if self.do_evaluate:
                    print('Callback: Evaluating...')

                    # Оценка модели
                    evaluation = evaluate(self.model)

                    print(f'Callback: Evaluation results: {evaluation}')
                    print(f'Callback: Serializing to JSON')

                    stat_path = os.path.join(output, 'eval_results.json')

                    # Сохранение результатов в формате JSON
                    with open(stat_path, 'w', encoding='utf-8') as file:
                        json.dump(evaluation, file)

                    print(f'Callback: Saved to {stat_path}')

            except FileNotFoundError:
                print('Callback: датасет для оценки не найден. Пропускаю')

            # Проверка на тестовом изображении
            if self.test_image is not None:

                print(f'Callback: Testing on image')

                test_path = os.path.join(output, 'test_image.png')

                # Получение предсказания
                prediction = predict(self.model, self.test_image)
                pred_path = os.path.join(output, 'test_prediction.png')

                # Сохранение предсказания и оригинала
                print(f'Callback: O: '
                      f'{cv.imwrite(test_path, self.test_image)}')
                print(f'Callback: P: '
                      f'{cv.imwrite(pred_path, prediction)}')

                print(f'Callback: Saved to {test_path} and {pred_path}')

            print(f'Callback: Continue training')


def train(*, data_path=None, output=None, do_test=True, test_path=None,
          eval_path=None, batch_size=BATCH_SIZE, epochs=100, save_period=5):
    """
    Тренировка модели с сохранением чекпоинтов и оценкой результатов

    :param data_path: Папка с датасетом (def. PATH_TRAIN)
    :param output: Папка для сохранения результатов (def. PATH_MODEL_OVERALL)
    :param do_test: Проводить ли проверку
    :param test_path: Путь к изображению для проверки
    :param eval_path: Путь к датасету для оценки
    :param batch_size: Размер группы
    :param epochs: Количество эпох
    :param save_period: Период сохранения
    """

    data_path = data_path or PATH_TRAIN     # Папка датасета
    eval_path = eval_path or PATH_EVAL
    output = output or PATH_MODEL   # Папка для сохранения чекпоинтов
    overall = os.path.join(output, 'overall')   # Папка для финальной модели

    if not os.path.exists(overall):  # Создание папки, если она не существует
        os.mkdir(overall)

    print(f'Main: Training model | batch_size={batch_size}; epochs={epochs}')

    # Создание модели
    model = UNet(image_shape)

    # Настройка колл-бэка для сохранения
    if do_test:
        # Загрузка тестового изображения
        test_path = test_path or os.path.join(PATH_TEST, 'Screenshot_2.png')
        test_image = cv.imread(test_path)

        if test_image is not None:
            sv = SavingCallback(epoch=save_period,
                                save_location=output,
                                test_image=test_image)

        else:
            sv = SavingCallback(epoch=save_period,
                                save_location=output)
            do_test = False

            print(f'Main: Тестовое изображение \'{test_path}\' не найдено. '
                  f'Проверка не будет производиться')
    else:
        sv = SavingCallback(epoch=save_period,
                            save_location=output)

        test_image = None

    # Обучение модели
    start = time()
    end = start
    print(f'Main: Train start at '
          f'{datetime.fromtimestamp(start):%d.%m.%y %H:%M:%S}')

    try:
        history, model = fit(model, data_path, history=True, verbose=True,
                             epochs=epochs, batch_size=batch_size,
                             callbacks=[sv])

    except FileNotFoundError:
        end = time()

        print(f'Main: Датасет для обучения не найден')
        print(f'Main: Убедитесь, что папка {data_path} существует '
              f'и содержит изображения в формате png')

        return

    except Exception:
        end = time()

        print(f'Main: Произошла неожиданная ошибка:')

        # format_exc + print вместо print_exc чтобы это всё попало в лог
        print(traceback.format_exc())

        return

    else:
        end = time()

        print(f'Main: finished training at '
              f'{datetime.fromtimestamp(end):%D %H:%M:%S}')

    finally:
        print(f'Main: Time elapsed '
              f'{datetime.utcfromtimestamp(end - start):%H:%M:%S}')

    # Сохранение весов
    print('Main: Saving weights')

    model.save_weights(os.path.join(overall, 'weights'))

    print('Main: Weights saved')
    print('Main: Evaluating...')

    # Оценка модели
    try:
        evaluation = evaluate(model, eval_path)
        print(f'Main: Evaluation results: {evaluation}')

    except FileNotFoundError:
        evaluation = 'Error'
        print(f'Main: Датасет для оценки из {eval_path} не найден. '
              f'Оценка производиться не будет')

    print(f'Main: Serializing to JSON')

    # Создание словаря статистики
    stats = {'history': history.history, 'evaluation': evaluation}
    stat_path = os.path.join(overall, 'statistics.json')

    # Сохранение статистики в формате JSON
    with open(stat_path, 'w', encoding='utf-8') as file:
        json.dump(stats, file)

    print(f'Main: Saved to {stat_path}')
    print(f'Main: Testing on image')

    test_path = os.path.join(overall, 'test_image.png')

    # Тестирование на изображении
    if do_test and test_image is not None:
        prediction = predict(model, test_image)
        pred_path = os.path.join(overall, 'test_prediction.png')

        # Сохранение предсказания и оригинала
        print(f'Main: O: {cv.imwrite(test_path, test_image)}')
        print(f'Main: P: {cv.imwrite(pred_path, prediction)}')

        print(f'Main: Saved to {test_path} and {pred_path}')
        print(f'Main: Plotting')

    # Построение графиков

    # Создание холста
    fig, (ax0, ax1, ax2) = plt.subplots(1, 3, figsize=(15, 5), dpi=100)

    # График потерь
    ax0.plot(stats['history']['loss'], label='Train loss')
    ax0.plot(stats['history']['val_loss'], label='Validation loss')
    ax0.set_xlabel('Epochs')
    ax0.set_ylabel('Loss')
    ax0.set_title('Model loss')
    ax0.legend()

    # График точности
    ax1.plot(stats['history']['accuracy'], label='Train accuracy')
    ax1.plot(stats['history']['val_accuracy'], label='Validation accuracy')
    ax1.set_xlabel('Epochs')
    ax1.set_ylabel('Accuracy')
    ax1.set_title('Model accuracy')
    ax1.legend()

    # График IoU (Пересечение к объединению)
    ax2.plot(stats['history']['iou'], label='Train IoU')
    ax2.plot(stats['history']['val_iou'], label='Validation IoU')
    ax2.set_xlabel('Epochs')
    ax2.set_ylabel('IoU')
    ax2.set_title('Model IoU')
    ax2.legend()

    # Сохранение графика в файл
    fig_path = os.path.join(overall, 'plots.png')
    plt.tight_layout(), plt.savefig(fig_path)

    print(f'Main: Plots saved to {fig_path}')

    return model


def test(*, weights_path=None, data_path=None, output=None,
         do_eval=False, eval_path=None):
    """
    Получает предсказания нейросети для заданного набора изображений

    :param weights_path: Путь для загрузки весов (вкл. название файла)
    :param data_path: Папка с изображениями для проверки
    :param output: Путь для сохранения результатов
    :param do_eval: Нужно ли проводить оценку
    :param eval_path: Путь для папки с оценочным датасетом
    """

    # Задание путей по умолчанию, если не заданы
    weights_path = weights_path or os.path.join(PATH_MODEL_OVERALL, 'weights')
    data_path = data_path or PATH_TEST
    output = output or PATH_OUTPUT

    # Создание модели и загрузка весов
    model = UNet(image_shape)
    model.load_weights(weights_path)

    # Оценка модели
    try:
        if do_eval:
            ev = evaluate(model, eval_path)
            print('Оценка модели:')
            print(ev)

    except FileNotFoundError:
        print(f'Датасет для оценки из {eval_path} не найден. '
              f'Оценка производиться не будет')

    # Предсказание результатов для изображений из выбранной папки
    try:
        for i, img in enumerate(images.load_bunch(
                os.path.join(data_path, '*.png')
        )):

            pred = predict(model, img)   # Получение предсказания
            img = images.crop(img, pred.shape)

            res = cv.hconcat([img, pred])

            # Сохранение изображения
            img_path = os.path.join(output, f'test_{i}.png')
            if not cv.imwrite(img_path, res):
                print(f'Не удалось сохранить файл {img_path}. '
                      f'Возможно файл уже существует')

    except FileNotFoundError:
        print(f'Изображения не найдены. Убедитесь, что папка {data_path} '
              f'существует и содержит изображения в формате png')


if __name__ == '__main__':
    # В начале файла заданы пути, с которыми работает программа.
    # Их можно изменить, чтобы не задавать необходимые пути как аргументы
    # при каждом вызове необходимых функций

    # Для запуска предназначены две функции - train и test

    # Функция train тренирует нейросеть в течение 'epochs' (100) эпох
    # на размеченном датасете (PATH_TRAIN по умолчанию), сохраняя промежуточные
    # результаты каждые 'save_period' (5) эпох в папку 'output' (PATH_MODEL).
    # Конечный результат сохраняется в папку overall в папке 'output'
    # train()

    # Функция test загружает веса модели (по умолчанию из PATH_MODEL_OVERALL)
    # и получает предсказания для набора изображений из заданной папки
    # (PATH_TEST) и сохраняет их (в PATH_OUTPUT). Если do_eval указан как True,
    # дополнительно проводится оценка нейросети на оценочном датасете(PATH_EVAL)
    test(do_eval=True)

    # Если при запуске возникнет ошибка OOM (Out of memory) можно попробовать
    # уменьшить значение BATCH_SIZE в начале этого файла

# Убеждаемся, что файл логов будет закрыт
logger.close()
